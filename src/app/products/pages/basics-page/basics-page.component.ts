import { Component } from '@angular/core';

@Component({
  selector: 'app-basics-page',
  templateUrl: './basics-page.component.html',
  styleUrl: './basics-page.component.css'
})
export class BasicsPageComponent {
  public nameLower='fernando';
  public nameUpper='HERRERA';
  public fullName='FeRNAndo HeRrErA'

  public date= new Date();
}

import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Heroe, Color } from '../../interfaces/hero.interface';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrl: './order.component.css'
})
export class OrderComponent {
  public isUpperCase:boolean=false;
  public valueSortBy!:keyof Heroe;
  public heroes:Heroe[]=[
    {
        name:'superman',
        canFly:true,
        color:Color.red
    },
    {
        name:'batman',
        canFly:false,
        color:Color.black
    },
    {
        name:'redDevil',
        canFly:false,
        color:Color.blue
    },
    {
        name:'robin',
        canFly:false,
        color:Color.green
    }
  ]

  toogleUpperCase(){
    this.isUpperCase=!this.isUpperCase;
    
  }

  valorAsignado(valor:keyof Heroe){
    this.valueSortBy=valor;
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { Heroe } from '../interfaces/hero.interface';

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  transform(value: Heroe[], sortBy:keyof Heroe): Heroe[] {
    
    switch(sortBy){
      case 'name':
        return value.sort((a,b)=>a.name >b.name? 1:-1);
      case 'canFly':
        return value.sort((a,b)=>a.canFly >b.canFly? 1:-1);
      case 'color':
        return value.sort((a,b)=>a.color >b.color? 1:-1);
      default:
        return value;
      
    }
  }

}
